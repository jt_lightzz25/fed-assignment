// JavaScript source code

function GetCost() {
    var ppl = document.getElementById("noPpl").value;
    var cost = ppl * 1500;
    var discount = "-";

    if (ppl === "3") {
        var finalCost = cost * 0.95;
        discount = "5%";
    }

    else if (ppl === "4") {
        finalCost = cost * 0.90;
        discount = "10%";
    }

    else if (ppl === "5") {
        finalCost = cost * 0.85;
        discount = "15%";
    }

    else {
        finalCost = cost;
    }

    document.getElementById('total').innerHTML = "$" + finalCost + "<br />Discount given: " + discount;
}

function Submit() {
    var name = document.getElementById('name').value;
    alert("Thank you " + name + ", have a nice day!");
}
