var slide = 1;
slides(slide);

function forward() {
    slides(slide += 1);
}

function backward(){
    slides(slide -= 1);
}

function slides(x) {
    var slidesshown = document.getElementsByClassName("slideshow");
    if (x > slidesshown.length)
        slide = 1;
    if (x < 1)
        slide = slidesshown.length;

    for (var i = 0; i < slidesshown.length; i++) {
        slidesshown[i].style.display = "none";
    }

    slidesshown[slide-1].style.display = "block";
}

function showsurvey() {
    document.getElementById("output").value = "";
    document.getElementById("glass").style.display = "block";
    document.getElementById("hidden").style.display = "block";
}

function removesurvey() {
    document.getElementById("glass").style.display = "none";
    document.getElementById("hidden").style.display = "none";
}

function showscore() {
    removesurvey();
    var score = 0;

    if (document.getElementsByName("q1")[0].checked)
        score += 1;

    if (document.getElementsByName("q2")[0].checked)
        score += 1;

    if (document.getElementById("q3").value === "Y" ||
        document.getElementById("q3").value === "y")
        score += 1;

    document.getElementById("output").innerHTML = "Your score is " + score + " out of 3! <br />";
    if (score === 0 || score === 1)
        document.getElementById("output").innerHTML += "Try harder next time!";
    else if (score === 2 || score === 3)
        document.getElementById("output").innerHTML += "Well done!";
}